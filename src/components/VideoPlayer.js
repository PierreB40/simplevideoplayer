import React from 'react';

export default class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);
    this.myVideo = React.createRef()
    this.myBar = React.createRef()
    this.state = {
      playing: true,
      muted: false,
      fullscreen: false,
      duration: {
        total: 0,
        minutes: '00',
        seconds: '00'
      },
      currentTime: {
        total: 0,
        minutes: '00',
        seconds: '00'
      }
    }
    this.handleUpdate = this.handleUpdate.bind(this);
    this.tooglePlay = this.tooglePlay.bind(this);
    this.toogleMute = this.toogleMute.bind(this);
    this.toogleFullscreen = this.toogleFullscreen.bind(this);
    this.seekToAPoint = this.seekToAPoint.bind(this);
  }

  componentWillMount() {
    let i = setInterval(() => {
      if (this.myVideo.current.readyState > 0) {
        const minutes = parseInt(this.myVideo.current.duration / 60, 10);
        const seconds = Math.round(this.myVideo.current.duration % 60);
        this.setState({
          duration: {
            total: this.myVideo.current.duration,
            minutes: this.pad(minutes),
            seconds: this.pad(seconds)
          }
        })
        clearInterval(i);
      }
    }, 200)

  }

  pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
  }

  tooglePlay() {
    if (this.state.playing) {
      this.myVideo.current.pause()
    } else {
      this.myVideo.current.play()
    }
    this.setState(state => ({
      playing: !state.playing
    }))
  }

  toogleMute() {
    if (this.state.muted) {
      this.myVideo.current.muted = false
    } else {
      this.myVideo.current.muted = true
    }
    this.setState(state => ({
      muted: !state.muted
    }))
  }

  toogleFullscreen() {
    if (this.myVideo.current.requestFullscreen) {
      this.myVideo.current.requestFullscreen();
    } else if (this.myVideo.current.mozRequestFullScreen) {
      this.myVideo.current.mozRequestFullScreen();
    } else if (this.myVideo.current.webkitRequestFullscreen) {
      this.myVideo.current.webkitRequestFullscreen();
    } else if (this.myVideo.current.msRequestFullscreen) {
      this.myVideo.current.msRequestFullscreen();
    }
    this.setState(state => ({
      fullscreen: !state.fullscreen
    }))
  }

  seekToAPoint() {
    this.myVideo.current.currentTime = this.myBar.current.value;
  }

  handleUpdate(event) {
    let minutes = parseInt(event.target.currentTime / 60, 10);
    let seconds = Math.round(event.target.currentTime % 60);
    if (seconds === 60) {
      minutes = 1;
      seconds = 0;
    }
    this.setState({
      currentTime: {
        total: event.target.currentTime,
        minutes: this.pad(minutes),
        seconds: this.pad(seconds)
      }
    })
    if (this.state.playing) {
      this.myBar.current.value = this.state.currentTime.total;
    }
  }
  render() {
    return (
      <div>
        <video ref={this.myVideo} id="video" autoPlay onTimeUpdate={this.handleUpdate} src="https://s3-eu-west-1.amazonaws.com/onrewind-test-bucket/big_buck_bunny.mp4" type="video/mp4"></video>
        <div className="content">
          <span>{this.state.currentTime.minutes}:{this.state.currentTime.seconds}</span>
          <span> / </span>
          <span>{this.state.duration.minutes}:{this.state.duration.seconds}</span>
          <input ref={this.myBar} onChange={this.seekToAPoint} type="range" min="0" max={this.state.duration.total}></input>
          <button id="playBtn" onClick={this.tooglePlay}>Play/Pause</button>
          <button id="muteBtn" onClick={this.toogleMute}>mute</button>
          <button id="fullscreenBtn" onClick={this.toogleFullscreen}>Fullscreen</button>
        </div>
      </div>
    );
  }
}
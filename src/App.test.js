import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure } from 'enzyme';
import ReactDOM from 'react-dom';
import App from './App';
import VideoPlayer from './components/VideoPlayer';

configure({ adapter: new Adapter() });

describe('VideoPlayer Component', () => {

  it('is muted', () => {
    const wrap = mount(<VideoPlayer></VideoPlayer>);
    wrap.find('#muteBtn').simulate('click');
    expect(wrap.state('muted')).toEqual(true);
  });

  it('is fullscreen', () => {
    const wrap = mount(<VideoPlayer></VideoPlayer>);
    wrap.find('#fullscreenBtn').simulate('click');
    expect(wrap.state('fullscreen')).toEqual(true);
  });

  it('is changed on seek', () => {
    const wrap = mount(<VideoPlayer></VideoPlayer>);
    wrap.find('input').simulate('change', { target: { value: 0.3 } });
    expect(wrap.find('#video').props().currentTime).toEqual(0.3);
  });
})

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
